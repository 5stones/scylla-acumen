import os
import cgi
import xml.etree.ElementTree as ET
from io import BytesIO
from scylla import configuration
from scylla import log

class RequestBuilder(object):
    def dict_to_xml(self, element, xmlroot):

        if isinstance(element, dict):
            for key in element:
                newnode = ET.SubElement(xmlroot, key)
                self.dict_to_xml(element[key], newnode)

        elif isinstance(element, list):
            for value in element:
                self.dict_to_xml(value, xmlroot)

        else:
            xmlroot.text = str(element)

    def get_add_rows_request(self, definition):
        tree = ET.parse(self.__get_add_rows_template())
        tree_root = tree.getroot()
        tree_root.set("xmlns:SOAP-ENC", 'http://www.w3.org/2003/05/soap-encoding')
        tree_root.set("xmlns:xsd", 'http://www.w3.org/2001/XMLSchema')
        tree_root.set("xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance')

        root = tree.getroot()[0][0]
        row = root.find('add_rows_request').find('rows').find('row')

        self.__set_request_auth(root)
        self.__set_table_and_view(root, 'ecommerce_order', 'full_list')

        # build columns based on dictionary
        columns = definition['columns']
        for column in columns:
            self.__add_column_to_node(row, definition['table'] + '.' + column, columns[column])

        return tree

    def __set_request_auth(self, root):
        auth = root.find('authentication')
        auth.find('site_code').text = configuration.get('Acumen', 'site_code')
        auth.find('password').text = configuration.get('Acumen', 'site_password')

    def __set_table_and_view(self, root, table_name, view_name):
        add_rows_request = root.find('add_rows_request')
        add_rows_request.find('table_name').text = table_name
        add_rows_request.find('view_name').text = view_name

    def __get_add_rows_template(self):
        return os.path.dirname(os.path.abspath(__file__)) + '/templates/add_rows_request.xml'

    def __add_column_to_node(self, node, name, value):
        column = ET.SubElement(node, 'column')
        col_name = ET.SubElement(column, 'column_name')
        col_name.text = name
        col_value = ET.SubElement(column, 'column_value')
        col_value.text = value


class OrderBuilder(RequestBuilder):

    def __get_order_template(self):
        return os.path.dirname(os.path.abspath(__file__)) + '/templates/order_template.xml'

    def get_order_request(self, order):
        ET.register_namespace('SOAP-ENV', 'http://www.w3.org/2003/05/soap-envelope')
        ET.register_namespace('', 'http://www.cyberwolf.com/ApplicationProcessor')

        order_config = configuration.get('Acumen', 'order')
        order_xml = self.__get_order_xml(order)

        definition = {
            'table': 'ecommerce_order',
            'view': 'full_list',
            'columns': {
                'from': order_config.get('email_from'),
                'to': order_config.get('email_to'),
                'subject': order_config.get('signature_string'),
                'message_type': 'Receive_Order',
                'message_site': configuration.get('Acumen', 'site_code'),
                'message_status': 'New',
                'body': order_xml
            }
        }

        tree = self.get_add_rows_request(definition)

        return ET.tostring(tree.getroot())

    def __get_order_xml(self, order):
        order_tree = ET.parse(self.__get_order_template())

        root = order_tree.getroot()
        self.dict_to_xml(order, root[0])

        f = BytesIO()
        order_tree.write(f, encoding='MacRoman', xml_declaration=True)

        return f.getvalue()

from scylla import tasks
from scylla import orientdb
from scylla import log
from . import client
from .records import AcumenRecord
from pprint import pprint


class UpdateAcumenTask(tasks.Task):
    client_classes = {
        'Product': client.AcumenClient,
    }

    timing_buffer = 5

    def __init__(self, record_type, id_field):
        task_id = '{0}-update'.format(record_type)
        super(UpdateAcumenTask, self).__init__(task_id)

        self.record_type = record_type
        self.id_field = id_field


class UpdateAcumenOrderTask(tasks.Task):
    """Update the ship status of existing open orders.
    """
    timing_buffer = 5

    id_field = 'OrderID'

    def __init__(self, record_type):
        self.record_type = record_type
        task_id = '{0}-update'.format(self.record_type)
        super(UpdateAcumenOrderTask, self).__init__(task_id)

class ToAcumenTask(tasks.ReflectTask):
    """Checks for updates to records an pushes them into acumen.
    """
    id_field = 'id'

    def __init__(self,
            from_class,
            conversion,
            acumen_client, to_type,
            where="out('Created').size() = 0",
            with_reflection=None):

        super(ToAcumenTask, self).__init__(
            from_class,
            ('Acumen', to_type),
            where=where,
            with_reflection=with_reflection)

        self.id_field = self.to_type + 'ID'
        self.conversion = conversion
        self.acumen_client = acumen_client

    def _process_response_record(self, obj):
        data = self.conversion(obj)

        acumen_order = self.acumen_client.create_order(data)

        acumen_result = self.acumen_client.send_order(acumen_order)


        data[self.id_field] = data['OrderNumber']

        # save the acumen response to orient and link it
        rec = AcumenRecord.factory(data, self.to_class)
        self._save_response(obj, rec, request=data)

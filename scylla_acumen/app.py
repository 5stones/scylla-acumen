from scylla import app
from . import tasks


class App(app.App):

    nap_length = 300
    main_options = [
        ('retry-errors', 'r'),
    ]

    def _prepare(self):
        self.tasks['order-update'] = tasks.UpdateAcumenOrderTask('AcumenOrder')

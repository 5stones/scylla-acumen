from suds.sudsobject import Object as SudsObject
import datetime
from scylla import records


class AcumenRecord(records.Record):
    """Manages an acumen record and its subrecords.
    """

    key_field = 'OrderNumber'
    classname = 'AcumenRecord'

    def __init__(self, obj, classname=None):
        """Use AcumenRecord.factory(obj) instead.
        """
        super(AcumenRecord, self).__init__(AcumenRecord.__suds_to_dict(obj))
        if classname:
            self.classname = classname
        else:
            self.classname = 'Acumen' + obj.__class__.__name__

    @classmethod
    def factory(cls, obj, classname=None):
        """Instantiates a AcumenRecord object using a subclass if one is available.

        Args:
            obj: A suds.sudsobject.Object that contains the data for this records, and its subrecords.
        """
        if not classname:
            classname = 'Acumen' + obj.__class__.__name__
        for subclass in cls.__subclasses__():
            if subclass.__name__ == classname:
                return subclass(obj, classname)

        return cls(obj, classname)

    @classmethod
    def __suds_to_value(cls, value):
        if isinstance(value, SudsObject):
            if hasattr(value, 'IPID'):
                value = cls.factory(value)
            else:
                value = cls.__suds_to_dict(value)
        elif isinstance(value, list):
            value[:] = [cls.__suds_to_value(v) for v in value]
        return value

    @classmethod
    def __suds_to_dict(cls, obj):
        if isinstance(obj, dict):
            return obj
        props = {}
        for name in dir(obj):
            value = getattr(obj, name)
            # only convert public properties, not functions
            if not name.startswith('__') and not callable(value):
                props[name] = cls.__suds_to_value(value)
        return props

    def query_ipid(self):
        # check key/value pairs for ipre id
        if not self.ipid and 'Ipre ID' in self.custom_data:
            self.ipid = self.custom_data['Ipre ID']

        return super(AcumenRecord, self).query_ipid()

    def process_results(self):
        return

    def __parse_name_value_pairs(self):
        if 'nameValues' in self.data:
            pairs = [pair.values() for pair in self.data['nameValues']]
            self.custom_data = dict(pairs)
        else:
            self.custom_data = {}

    def __process_null_dates(self, obj):
        for key, value in obj.iteritems():
            if isinstance(value, dict):
                self.__process_null_dates(value)
            elif isinstance(value, list):
                for el in value:
                    if isinstance(el, dict) or isinstance(el, list):
                        self.__process_null_dates(el)
            elif (isinstance(value, datetime.datetime) or isinstance(value, datetime.date)) and str(value) < '1900-01-01':
                obj[key] = None

class AcumenOrder(AcumenRecord):
    key_field = "OrderNumber"

    def process_results(self):
        self.__process_ship_status()
        self.__process_payment_status()

    def __process_ship_status(self):
        # check & save ship status
        if not self.data['IsMoreToShip']:
            self.data['_ship_status'] = 'shipped'
        else:
            self.data['_ship_status'] = 'ready'

    def __process_payment_status(self):
        # check & save payment status
        if self.data['BalanceAmount'] <= 0:
            self.data['_payment_status'] = 'paid'
        else:
            self.data['_payment_status'] = 'unpaid'
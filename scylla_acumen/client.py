from .builder import *
from scylla import configuration
from scylla import log
from scylla import orientdb
from scylla import RestClient

configuration.setDefaults('Acumen', {
    'url': None,
    'site_code': None,
    'site_password': None,
    'site_id': None,
    'order': {
        'signature_string': None,
        'email_to': None,
        'email_from': None
    },
    'debug': False,
})

class AcumenClientError(Exception):
    pass


class AcumenClient(RestClient):

    def __init__(self):
        self.site_code = configuration.get('Acumen', 'site_code')
        self.site_password = configuration.get('Acumen', 'site_password')
        self.site_id = configuration.get('Acumen', 'site_id')
        super(AcumenClient, self).__init__(configuration.get('Acumen', 'url'),
            verify_ssl=False,
            debug=configuration.get('Acumen', 'debug'))


    def _process_response(self, response):
        if response.status_code not in self._successful_codes:
            raise self.RestError(response.text)
        return response

    def create_order(self, order):
        ob = OrderBuilder()

        return ob.get_order_request(order)

    def send_order(self, acumen_order):
        acumen_order = acumen_order.encode('utf-8')
        self._set_header('Accept', '*')
        self._set_header('Content-Type', 'text/xml charset=utf8')
        self._set_header('Timeout', str(10 * 1000))
        self._set_header('Content-Length', str(len(acumen_order)))
        return self.post('AddRows', None, acumen_order)

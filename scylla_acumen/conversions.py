from datetime import datetime
from scylla import convert
from scylla import configuration


__config = configuration.getSection('Acumen', {
    'datetime_format': '%Y%m%d',
})


def to_acumen_date(value):
    """Change a datetime into a Acumen style datetime."""
    if value is None:
        return None
    if not isinstance(value, datetime):
        value = convert.parse_iso_date(value)
    if value.tzinfo:
        fixed_tz = convert.FixedTz(__config['timezone'])
        value = value.astimezone(fixed_tz)
    return value.strftime(__config['datetime_format'])

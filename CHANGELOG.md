<a name="1.2.0"></a>
# [1.2.0](https://gitlab.com/5stones/scylla-acumen/compare/v1.1.2...v1.2.0) (2018-12-04)


### Bug Fixes

* **setup.py:** Fix issue with typo in dependency URL ([2803564](https://gitlab.com/5stones/scylla-acumen/commit/2803564))


### Features

* **AcumenClient:** Add the ability to configure debugging ([b74a36a](https://gitlab.com/5stones/scylla-acumen/commit/b74a36a))



<a name="1.1.2"></a>
## [1.1.2](https://gitlab.com/5stones/scylla-acumen/compare/v1.1.1...v1.1.2) (2018-10-12)


### Bug Fixes

* **setup.py:** Fix issue with templates/*.xml not being included on installs ([6dc9145](https://gitlab.com/5stones/scylla-acumen/commit/6dc9145))



<a name="1.1.1"></a>
## [1.1.1](https://gitlab.com/5stones/scylla-acumen/compare/v1.1.0...v1.1.1) (2018-10-12)


### Bug Fixes

* **requirements.txt, setup.cfg:** Fix issue with missing or incorrect dependencies ([3c98181](https://gitlab.com/5stones/scylla-acumen/commit/3c98181))



<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/5stones/scylla-acumen/compare/v1.0.0...v1.1.0) (2018-10-12)



<a name="1.0.0"></a>
# [1.0.0](https://gitlab.com/5stones/scylla-acumen/compare/7b24650...v1.0.0) (2018-02-14)


### Bug Fixes

* **build.py:** Remove unneeded comments ([b6ab469](https://gitlab.com/5stones/scylla-acumen/commit/b6ab469))


### Features

* Initial Commit - Add basic xml request creation ([7b24650](https://gitlab.com/5stones/scylla-acumen/commit/7b24650))
* **client, builder:** Build order request from order dictionary and config ([9bd2787](https://gitlab.com/5stones/scylla-acumen/commit/9bd2787))
* **scylla_acumen/*:** Finish building out ACUMEN client, records, and builder ([40a3b2d](https://gitlab.com/5stones/scylla-acumen/commit/40a3b2d))
* **tasks.py:** Add where clause to not push bookfairs already in Elan ([401b7bf](https://gitlab.com/5stones/scylla-acumen/commit/401b7bf))




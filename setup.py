from distutils.core import setup
import json

with open('package.json', 'r') as f:
    package_json = json.load(f)

version = package_json['version']

setup(
    name = 'scylla-acumen',
    packages = ['scylla_acumen'],
    version = version,

    description = 'Scylla-Acumen provides the basic utilities to integrate with the standard acumen API',

    #author = '',
    #author_email = '',

    url = 'https://git@gitlab.com:5stones/scylla-acumen',
    download_url = 'https://gitlab.com/5stones/scylla-acumen/repository/archive.tar.gz?ref=' + version,

    keywords = 'integration scylla acumen',

    classifiers=[
        #'Development Status :: 4 - Beta',
        #'Development Status :: 5 - Production/Stable',

        #'Intended Audience :: Developers',
        #'Topic :: Software Development :: Build Tools',

        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],

    package_data = {
        'scylla_acumen': ['templates/*.xml']
    },

    install_requires = [
        'suds',
        'python-dateutil',
        'scylla',
    ],
    dependency_links=[
        'git+https://gitlab.com/5stones/scylla.git',
        'requests[security]',
    ],
)
